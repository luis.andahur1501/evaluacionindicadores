const fs = require('fs');
const fetch = require('node-fetch');
const readline = require('readline');
const timestamp = require('time-stamp');
var indicadorEscogido = '';
var lector = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
var minimo=9999;
var fechaMinimo='';
var maximo=0;
var fechaMaximo='';
var fechas = [];
const menu = (opcion) => {
    switch (opcion) {
        case '1':
            console.log('Opcion 1');
            //fetch('https://mindicador.cl/api').then(status).then(obtenerJson).then(filtrarDatos).then((datos)=>{fs.writeFile("datos/"+String(timestamp('DDMMYYYY-HHmmss'))+'.ind',JSON.stringify(datos),(err)=>{}); 
            //}).then(leerOpcion(lector).then(menu)).catch(console.error);
            getFile('../EvaluacionIndicadores/api.json').then(filtrarDatos).then((datos) => {
                fs.writeFile("datos/" + String(timestamp('DDMMYYYY-HHmmss')) + '.ind', JSON.stringify(datos), (err) => {});
            }).then(() => {
                leerOpcion(lector).then(menu)
            }).catch(console.error);

            break;
        case '2':
            /*Promedio*/
            leerIndicador(lector).then(dirArchivos).then(calcularPromedio).then(mostrarPromedio).then(() => {
                leerOpcion(lector).then(menu)
            }).catch(console.error);

            break;
        case '3':
            /*Valor Actual*/
            ultimoArchivo().then(console.log).then(() => {
                leerOpcion(lector).then(menu)
            }).catch(console.error);

            break;
        case '4':valorHistorico().then(console.log).catch(console.log);
        
            break;
        case '5':
            leerIndicador(lector).then(menu);
            break;
        case '6':
            process.exit(0);
            //leerIndicador(lector).then((indicador)=>{indicadorEsc = indicador}).then(()=>{cincoArchivos()}).catch(console.error);

            break;
        default:
            console.log('Opcion no encontrada');
            leerOpcion(lector).then(menu).catch(console.error);
            break;
    }
}
const filtrarDatos = (data) => {
    return new Promise((resolve, reject) => {
        var json = JSON.parse(data);
        var fecha = timestamp('DD-MM-YY');
        var hora = timestamp('HH:mm:ss');
        var info = '{"fecha":' + JSON.stringify(fecha) + ',';
        info += '"hora":' + JSON.stringify(hora) + ',';
        for (var elem in json) {
            /*if(elem==='fecha' ){
                info+=JSON.stringify(elem)+':'+JSON.stringify(json[elem])+',';
            }*/
            if (elem === 'dolar' || elem === 'euro') {
                //info+='"'+elem+'":{valor:"'+json[elem].valor+'"},';
                info += JSON.stringify(elem) + ':' + JSON.stringify(json[elem]) + ',';
            }
            if (elem === 'tasa_desempleo') {
                //info+='"'+elem+'":{valor"'+json[elem].valor+'},"';
                info += JSON.stringify(elem) + ':' + JSON.stringify(json[elem]);
            }
        }
        info += '}';
        console.log(json);
        resolve(JSON.parse(info));
    });
}

const dirArchivos = (indicador) => {
    return new Promise((resolve, reject) => {
        var path = '../EvaluacionIndicadores/datos';
        var valores = [];
        indicadorEscogido = indicador;
        while (fechas.length) {
            fechas.pop();
        }
        fs.readdir(path, (err, files) => {
            var tam = files.length;
            files.forEach((file) => {
                //console.log(file);
                getFile(path + '/' + file).then(JSON.parse).then(obtenerFecha).then((fecha) => {
                    fechas.push(fecha);
                }).catch(console.error);
                valores.push(getFile(path + '/' + file).then(JSON.parse).then(obtenerValor).catch(console.error));
            });
            resolve(Promise.all(valores));
        });
    });
};

const valorHistorico = () => {
    return new Promise((resolve, reject) => {
        var path = '../EvaluacionIndicadores/datos';
        var valores = [];
        while (fechas.length) {
            fechas.pop();
        }
        fs.readdir(path, (err, files) => {
            var tam = files.length;
            files.forEach((file) => {
                valores.push(getFile(path + '/' + file).then(JSON.parse).then(obtenerValorMinimo).catch(console.error));
            });
            resolve(Promise.all(valores));
        });
    });
};
const ultimoArchivo = () => {
    return new Promise((resolve, reject) => {
        var path = '../EvaluacionIndicadores/datos';
        var valores = [];
        while (fechas.length) {
            fechas.pop();
        }
        fs.readdir(path, (err, files) => {
            resolve(getFile(path + '/' + files[files.length - 1]).then(JSON.parse).then((json) => {
                return new Promise((resolve, reject) => {
                    var fecha = json['fecha'];
                    var hora = json['hora'];
                    var data = '';
                    data += 'Fecha y hora de guardado: ' + fecha + ' ' + hora + ' Valor actual del Dolar: ' + json['dolar'].valor;
                    data += ' Valor actual del Euro: ' + json['euro'].valor + ' Valor actual de Tasa Desempleo: ' + json['tasa_desempleo'].valor;
                    resolve(data);
                });
            }).catch(console.error));
        });
    });
};

function calcularPromedio(resultados) {
    return new Promise((resolve, reject) => {
        var suma = 0;
        var size = resultados.length;
        for (var i = 0; i < size; i++) {
            suma += resultados[i];
        }
        resolve((suma / size));
    });
}
const leerArchivo = (fullPath) => {
    return new Promise((resolve, reject) => {
        resolve(getFile(fullPath).then(JSON.parse).then(obtenerValor).catch(console.error));
    });
}
const mostrarPromedio = (prom) => {
    console.log(fechas);
    fechas.sort(comparar);
    console.log(fechas);
    var info = '';
    var primero = fechas[0];
    var ultimo = fechas[fechas.length - 1];
    info += 'Promedio: ' + prom + ' Rango de fechas: ' + primero + ' ' + ultimo;
    console.log(info);
}
const obtenerValorMinimo= (json)=>{
    return new Promise((resolve, reject) => {
        var arreglo=[];
        if(json['euro'].valor<minimo){
            minimo=json['euro'].valor;
            fechaMinimo= json['fecha'];
        }
        var info= 'Euro a $'+minimo+' el '+fechaMinimo;
        resolve(info);
    });
}

const obtenerValor = (json) => {
    return new Promise((resolve, reject) => {
        let valor = 0;
        if (indicadorEscogido == '') {
            console.log("Ingrese indicador valido");
            reject((new Error("Ingrese indicador valido")));
        }
        valor = json[indicadorEscogido].valor;
        resolve(json[indicadorEscogido].valor);
    });
};
const obtenerFecha = (json) => {
    return new Promise((resolve, reject) => {
        let valor = 0;
        if (indicadorEscogido == '') {
            console.log("Ingrese indicador valido");
            reject((new Error("Ingrese indicador valido")));
        }
        resolve(json['fecha']);
    });
};

const getFile = fileName => {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data);
        });
    });
}

const status = response => {
    if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response);
    }
    return Promise.reject(new Error(response.statusText));
};
const obtenerJson = response => {
    console.log(response);
    return response.json();
};
const leerOpcion = (lectura) => {
    return new Promise((resolve, reject) => {
        console.log('Menu');
        console.log('1.- Actualizar Datos');
        console.log('2.- Promediar');
        console.log('3.- Mostar Valor más actual ');
        console.log('4.- Mostrar Minimo Historico');
        console.log('5.- Mostrar Minimo Historico');
        console.log('6.- Salir');
        lectura.question('Escriba su opcion: ', opcion => {
            resolve(opcion);
        });
    });
}

const leerIndicador = (lectura) => {
    return new Promise((resolve, reject) => {
        console.log('Indicadores: ');
        console.log('- dolar');
        console.log('- euro');
        console.log('- tasa_desempleo');
        lectura.question('Escoja un indicador: ', indicador => {
            resolve(indicador);
        });
    });
}

function comparar(a, b) {
    var comp1 = a.split('-');
    var comp2 = b.split('-');

    if (comp1[2] > comp2[2]) {
        return 1;
    } else if (comp1[2] == comp2[2] && comp1[1] > comp2[1]) {
        return 1;
    } else if (comp1[2] == comp2[2] && comp1[1] == comp2[1] && comp1[0] > comp2[0]) {
        return 1;
    }

    if (comp1[2] < comp2[2]) {
        return -1;
    } else if (comp1[2] == comp2[2] && comp1[1] < comp2[1]) {
        return -1;
    } else if (comp1[2] == comp2[2] && comp1[1] == comp2[1] && comp1[0] < comp2[0]) {
        return -1;
    }
    return 0;
}
leerOpcion(lector).then(menu).catch(console.error);